﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edabitChallenge
{
    class ArrayMultiples
    {
         public static int[] ArrayOfMultiples(int num, int length)
        {
            int[] array = new int[length];

            for (int i =0 ; i < length; i++)
            {
                array[i] = num * (i+1);
            }

            return array;
        }
        
    }
}
