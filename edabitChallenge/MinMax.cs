﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edabitChallenge
{
    public class MinMax
    {
        public static double[] FindLargest(double[][] values)
        {
            return values.Select(v => v.Max()).ToArray();


        }
    }

}
