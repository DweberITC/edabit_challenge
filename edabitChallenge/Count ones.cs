﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edabitChallenge
{
    class CountingOnes
    {
        public static int CountOnes(int i)
        {
            int[] p = new int[8];
            string pa = "";
            for (int ii = 0; ii <= 7; ii = ii + 1)
            {
                p[7 - ii] = i % 2;
                i = i / 2;
            }
            for (int ii = 0; ii <= 7; ii = ii + 1)
            {
                pa += p[ii].ToString();
            }

            int count = pa.Count(f => f == '1');

            return count;

        }
    }
}
