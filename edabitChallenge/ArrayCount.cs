﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edabitChallenge
{
    public class ArrayCount
    {

        public static int CountTrue(bool[] arr)
        {
            int ausgabe = arr.Count(b => b == true);
            return ausgabe;

        }

    }
}
