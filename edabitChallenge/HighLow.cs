﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edabitChallenge
{
    class HighestLowest
    {
        public static string HighLow(string str)
        {
            var arr = str.Split(' ').Select(int.Parse).ToArray();
            return arr.Max() + " " + arr.Min();
            
        }
    }
}
