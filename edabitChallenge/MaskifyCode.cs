﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edabitChallenge
{
    class MaskifyCode
    { 
        public static string Maskify(string str)
        {
            int l = str.Length;
            int a = l - 4;
            return String.Concat(Enumerable.Repeat("#", a)) + str.Substring(a);



        }
    }
}
