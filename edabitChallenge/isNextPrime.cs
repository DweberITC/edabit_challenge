﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edabitChallenge
{
    public class isNextPrime
    {
        public static int NextPrime(int num)
        {
            int m, i = 0;
            bool flag = true;
            m = num / 2;
            do
            {
                flag = true;

                for ( i = 2; i <= m; i++)
                {
                    if (num % i == 0)
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag == true)
                {
                    return num;
                }

                num++;

            } while (flag == false);
            return 404;
        }
    }
}
