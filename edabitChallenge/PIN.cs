﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edabitChallenge
{
    class PIN
    {
        public static bool ValidatePIN(string pin)
        {
            bool ausgabe = false;
            char[] charArr = pin.ToCharArray();
            if (pin.Length == 4 )
            {
                ausgabe = true;
                foreach (char a in charArr)
                {
                    if (char.IsDigit(a) != true)
                    { 
                        ausgabe = false;
                    }
                    
                }

            }

            if (pin.Length == 6)
            {
                ausgabe = true;
                foreach (char a in charArr)
                {
                    if (char.IsDigit(a) != true)
                    {
                        ausgabe = false;
                    }

                }

            }
            return ausgabe;
        }
    }
}
