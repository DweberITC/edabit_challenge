﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edabitChallenge
{
    public class StringReplace
    {
        public static string HackerSpeak(string str)
        {
            string ausgabe = str.Replace('a', '4').Replace('e', '3').Replace('i', '1').Replace('o', '0').Replace('s', '5');
            return ausgabe;
        }
    }
}
